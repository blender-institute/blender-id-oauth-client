#!/bin/bash

VERSION="${1/version-}"

if [ -z "$VERSION" ]; then
    echo "Usage: $0 new-version" >&2
    exit 1
fi

poetry version $VERSION

git diff
echo
echo "Don't forget to commit and tag:"
echo git commit -m \'Bumped version to $VERSION\' pyproject.toml
echo git tag -a version-$VERSION -m \'Tagged version $VERSION\'
echo
echo "Build the package & upload to PyPi using:"
echo poetry build
echo poetry publish
