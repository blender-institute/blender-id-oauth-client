# Releasing new versions

- Run `update_version.sh 1.0`, commit, and push (including tags).
- Run `poetry run py.test` to run the tests one final time.
- Run `poetry build` to create distribution files.
- Run `poetry publish` to publish the distribution files to Pypi.
