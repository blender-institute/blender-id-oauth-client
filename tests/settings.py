"""Django settings for tests."""
# Adapted from the Django Debug Toolbar test settings.py

import pathlib

BASE_DIR = pathlib.Path(__file__).absolute().parent

# Quick-start development settings - unsuitable for production
SECRET_KEY = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
INTERNAL_IPS = ['127.0.0.1']
LOGGING_CONFIG = None   # avoids spurious output in tests


# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blender_id_oauth_client',
    'tests',
]

MEDIA_URL = '/media/'   # Avoids https://code.djangoproject.com/ticket/21451

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'tests.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

STATIC_ROOT = BASE_DIR / 'tests' / 'static'

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    BASE_DIR / 'tests' / 'additional_static',
]

# Cache and database
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
}
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'blender-id-oauth-client.db',
    }
}

BLENDER_ID = {
    # MUST end in a slash:
    'BASE_URL': 'https://id.local:8400/',
    'OAUTH_CLIENT': 'TEST-CLIENT-ID',
    'OAUTH_SECRET': 'TEST-SECRET'
}


LOGIN_URL = '/oauth/login'
LOGOUT_URL = '/oauth/logout'
LOGIN_REDIRECT_URL = '/'

import os
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'  # In production this must be 0 or not set


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Amsterdam'  # This influences rendering in templates.
USE_TZ = True  # This causes all datetimes to be UTC in the database.

USE_I18N = True
USE_L10N = False
DATE_FORMAT = 'l Y-b-d'
TIME_FORMAT = 'H:i:s'
DATETIME_FORMAT = f'{DATE_FORMAT}, {TIME_FORMAT}'
SHORT_DATE_FORMAT = 'Y-m-d'
SHORT_DATETIME_FORMAT = f'{SHORT_DATE_FORMAT} H:i'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
