from django.contrib.auth.decorators import login_required
from django.http import JsonResponse


@login_required
def protected_view(request):
    return JsonResponse({'user_id': request.user.id})
